/* eslint no-console: 0 */
// Run this example by adding <%= javascript_pack_tag 'hello_vue' %> (and
// <%= stylesheet_pack_tag 'hello_vue' %> if you have styles in your component)
// to the head of your layout file,
// like app/views/layouts/application.html.erb.
// All it does is render <div>Hello Vue</div> at the bottom of the page.

import Vue from 'vue/dist/vue.esm'

document.addEventListener('DOMContentLoaded', () => {
  Vue.component('customercomponent',{
    template : '<div class = "Table"><table><tr v-bind:style = "styleobj"><td>{{itr.fname}}</td><td>{{itr.lname}}</td><td>{{itr.addr}}</td><td><button v-on:click = "$emit(\'removeelement\')">X</button></td></tr></table></div>',
    props: ['itr', 'index'],
    data: function() {
        return {
          styleobj : {
              backgroundColor:this.getcolor(),
              fontSize : 18
          }
        }
    },
    methods:{
        getcolor : function() {
          if (this.index % 2) {
              return "#efefef";
          } else {
              return "#ffffff";
          }
        }
    }
    });
    var vm = new Vue({
    el: '#app',
    data: {
        fname:'',
        lname:'',
        addr : '',
        customer:[]
    },
    methods :{
        showdata : function() {
          this.customer.push({
              fname: this.fname,
              lname: this.lname,
              addr : this.addr
          });
          this.fname = "";
          this.lname = "";
          this.addr = "";
        }
    }
    });
})


// The above code uses Vue without the compiler, which means you cannot
// use Vue to target elements in your existing html templates. You would
// need to always use single file components.
// To be able to target elements in your existing html/erb templates,
// comment out the above code and uncomment the below
// Add <%= javascript_pack_tag 'hello_vue' %> to your layout
// Then add this markup to your html template:
//
// <div id='hello'>
//   {{message}}
//   <app></app>
// </div>


// import Vue from 'vue/dist/vue.esm'
// import App from '../app.vue'
//
// document.addEventListener('DOMContentLoaded', () => {
//   const app = new Vue({
//     el: '#hello',
//     data: {
//       message: "Can you say hello?"
//     },
//     components: { App }
//   })
// })
//
//
//
// If the project is using turbolinks, install 'vue-turbolinks':
//
// yarn add vue-turbolinks
//
// Then uncomment the code block below:
//
// import TurbolinksAdapter from 'vue-turbolinks'
// import Vue from 'vue/dist/vue.esm'
// import App from '../app.vue'
//
// Vue.use(TurbolinksAdapter)
//
// document.addEventListener('turbolinks:load', () => {
//   const app = new Vue({
//     el: '#hello',
//     data: () => {
//       return {
//         message: "Can you say hello?"
//       }
//     },
//     components: { App }
//   })
// })
